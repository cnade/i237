#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define STUDENT_NAME "Simo Sirkas"

#define VER_FW "Version: " FW_VERSION " built on: " __DATE__" "__TIME__
#define VER_LIBC_GCC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__
#define UPTIME "Uptime"
extern PGM_P const numbers[];

#endif /* _HMI_MSG_H_ */
