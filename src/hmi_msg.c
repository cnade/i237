#include <avr/pgmspace.h>
#include "hmi_msg.h"

static const  char nr_1[] PROGMEM = "One";
static const  char nr_2[] PROGMEM = "Two";
static const  char nr_3[] PROGMEM = "Three";
static const  char nr_4[] PROGMEM = "Four";
static const  char nr_5[] PROGMEM = "Five";
static const  char nr_6[] PROGMEM = "Six";
static const  char nr_7[] PROGMEM = "Seven";
static const  char nr_8[] PROGMEM = "Eight";
static const   char nr_9[] PROGMEM = "Nine";
static const   char nr_0[] PROGMEM = "Zero";

PGM_P const numbers[] PROGMEM = {nr_0, nr_1, nr_2, nr_3, nr_4, nr_5, nr_6, nr_7, nr_8, nr_9};


