#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "uart_wrapper.h"
#include "hmi_msg.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "cli_microrl.h"
#ifndef BEL
#define BEL 0x07
#endif
#define UART_BAUD 9600

static microrl_t rl;
static microrl_t *prl = &rl;

//counter
volatile uint32_t counter_1;

static inline void init_leds(void)
{
    DDRA |= _BV(PA0);
    DDRA |= _BV(PA2);
    DDRA |= _BV(PA4);
    DDRB |= _BV(DDB7);
    PORTB &= ~_BV(PORTB7);
}



static inline void init_counter(void)
{
    counter_1 = 0;
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}


static inline void print_start_data(void)
{
    /* Print version libc and gcc info */
    fprintf_P(stderr, PSTR(VER_FW "\n"));
    fprintf_P(stderr, PSTR(VER_LIBC_GCC "\n"));
    /* Print student name*/
    fprintf_P(stdout, PSTR(STUDENT_NAME "\n"));
fprintf_P(stdout, PSTR(">"));
    /* Display student name in LCD */
    lcd_puts_P(PSTR(STUDENT_NAME));
}

static inline void heartbeat(void)
{
    static uint32_t time_y2k_prev;
    uint32_t time_y2k_cpy;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        time_y2k_cpy = counter_1;
    }

    if (time_y2k_cpy != time_y2k_prev) {
        PORTA ^= _BV(PA0);
	PORTA |= _BV(PA4);
        fprintf_P(stderr, PSTR(UPTIME": %lu s\n"), time_y2k_cpy);
        time_y2k_prev = time_y2k_cpy;
    }
}
void print(const char *str)
{
    printf("%s", str);
}


char get_char(void)
{
    if (uart0_peek() != UART_NO_DATA) {
        return uart0_getc() & 0x00FF;
    } else {
        return 0x00;
    }
}


void main(void)
{
    init_leds();
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    stdout = stdin = &uart0_io;
    stderr = &uart3_out;
    microrl_init (prl, print);
    microrl_set_execute_callback (prl, cli_execute);
    lcd_init();
    lcd_clrscr();
    init_counter();
    print_start_data();


    while (1) {
        heartbeat();
        microrl_insert_char (prl, get_char());
    }
}


ISR(TIMER1_COMPA_vect)
{
    counter_1++;
}
